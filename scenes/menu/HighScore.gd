extends AcceptDialog

func _ready():
	connect("about_to_show", self, "__refresh")
	$HTTPRequest.connect("request_completed", self, "__on_request_ready")
	pass # Replace with function body.

func __refresh():
	$Container/ItemList.clear()
	$HTTPRequest.request("https://line.hunter99.ch/game/score.php?action=get")
	
func __on_request_ready(result, response_code, headers, body):
	var body_pool = body.get_string_from_utf8().split("\n")
	var item = ""
	var i = 0
	$Container/ItemList.add_item("[Name]")
	$Container/ItemList.add_item("[Score]")
	$Container/ItemList.add_item("[Stage]")
	if body_pool.size() > 0:
		for part in body_pool:
			$Container/ItemList.add_item(part)
			if i < 3:
				item += part + "   "
				i += 1
			else:
				#$Container/ItemList.add_item(item)
				item = part + "   "
				i = 1