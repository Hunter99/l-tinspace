extends VSlider

var sound_level = 100
var sound_settings = "user://sound_settings.cnf"
var file : File

func _ready():
	file = File.new()
	if file.file_exists(sound_settings):
		file.open(sound_settings, File.READ)
		sound_level = file.get_16()
		file.close()
	value = sound_level
	update_sound(value)

func _on_VSlider_value_changed(value):
	update_sound(value)

func save_value():
	file.open(sound_settings, File.WRITE)
	file.store_16(sound_level)
	file.close()

func update_sound(value):
	var scaled_value = (value - 100) / 2
	if value <= 0:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), true)
	else:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), false)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), scaled_value)
	sound_level = value
	save_value()