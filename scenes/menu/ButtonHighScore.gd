extends Button

func _ready():
	self.connect("button_down", self, "__clicked")
	
func __clicked():
	get_parent().get_node("HighScore").popup()