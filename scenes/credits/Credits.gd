extends Position2D

export var speed = 50

func _ready():
	pass # Replace with function body.
	
func _process(delta):
	translate(Vector2.UP * speed * delta)
	if Input.is_action_just_pressed("escape"):
		get_tree().change_scene("res://scenes/menu/menu.tscn")
	if position.y < -$Credits.margin_bottom:
		get_tree().change_scene("res://scenes/menu/menu.tscn")