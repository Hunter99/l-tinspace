extends Area2D

export var speed = 100
export var damage = 20

var direction : Vector2 = Vector2.ZERO

var screen = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))

func _ready():
	connect("area_entered", self, "__collision")
	$AudioStreamPlayer.play()

func _process(delta):
	translate(direction * speed * delta)
	if position.y > screen.y + 50 or position.y < -50:
		queue_free()
	

func set_target(target):
	self.direction = target - position
	self.direction = self.direction.normalized()
	
func set_direction(direction):
	self.direction = direction.normalized()
	
func __collision(area):
	if area.has_method("get_type"):
		if area.get_type() == "Player":
			area.damage(damage)