extends Label

var points = 0

func _ready():
	add_points(0)
	pass # Replace with function body.

func add_points(value):
	points += value
	text = "Score: " + str(points)
	
func get_score():
	return points