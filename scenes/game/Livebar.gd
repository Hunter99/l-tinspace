extends Sprite

export(NodePath) var LiveNodePath

var init_x_size

func _ready():
	init_x_size = scale.x
	pass # Replace with function body.

func _process(delta):
	var live = get_node(LiveNodePath).get_live()
	scale.x = init_x_size / 100 * live