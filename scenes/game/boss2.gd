extends "res://scenes/game/CreatureClass.gd"

export var live = 100
export var speed = 75

var screen = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))
var shooting = false
onready var enemy_shot =  preload("res://scenes/game/enemy_shot.tscn")

func _ready():
	$Area2D.connect("area_entered", self, "__collision")
	$Area2D/Sprite.frame = 0
	pass # Replace with function body.
	
func _process(delta):
	if live <= 0:
		die()
	if position.y < screen.y / 3:
		translate(Vector2.DOWN * speed * delta)
	elif not shooting:
		$AnimationPlayer.play("shoot")
		shooting = false
		
func die():
	.die()
	get_node("/root/game/UI/LabelPoints").add_points(800)
	queue_free()
	
func reset_shooting():
	shooting = false
	
func shoot():
	var shots = []
	var max_shots = 40
	for i in range(max_shots):
		var shot = enemy_shot.instance()
		shot.position = position
		var direction = Vector2.RIGHT
		direction = direction.rotated(2 * PI / max_shots * i)
		shot.set_direction(direction)
		get_parent().add_child(shot)
		
func damage(value):
	live -= value / 80
	
func get_live():
	return live
	
func __collision(area):
	if area.has_method("get_type"):
		if area.get_type() == "Player":
			area.damage(20)