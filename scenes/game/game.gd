extends Node2D

export var enemy_spawn_time = 1
export var stage = 0

var enemy_spawn_timer : Timer
onready var enemy = preload("res://scenes/game/enemy.tscn")
var screen = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))

func _ready():
	enemy_spawn_timer = Timer.new()
	enemy_spawn_timer.wait_time = enemy_spawn_time
	enemy_spawn_timer.connect("timeout", self, "__spawn_enemy")
	enemy_spawn_timer.start()
	add_child(enemy_spawn_timer)
	$BossTimer.connect("timeout", self, "__spawn_boss")
	$BossTimer.start()
	$UI/LabelStage.set_stage(0)
	$UI/LabelGameOver.set("custom_colors/font_color", Color(1,1,1,0))

func __spawn_enemy():
	var x = rand_range(10, ProjectSettings.get("display/window/size/width"))
	var y = -100
	var enemy_instance = enemy.instance()
	enemy_instance.position = Vector2(x, y)
	add_child(enemy_instance)

func _process(delta):
	if Input.is_action_just_pressed("escape"):
		get_tree().change_scene("res://scenes/menu/menu.tscn")
		
func __spawn_boss():
	# 4, 7, 10
	if stage < 4:
		var boss : CreatureClass 
		boss = preload("res://scenes/game/boss1.tscn").instance()
		boss.position.y = -100
		boss.position.x = rand_range(100, screen.x - 100)
		boss.connect("dying", self, "__reset_boss_timer")
		add_child(boss)
	elif stage < 7:
		var boss_1 : CreatureClass 
		var boss_2 : CreatureClass 
		boss_1 = preload("res://scenes/game/boss1.tscn").instance()
		boss_2 = preload("res://scenes/game/boss1.tscn").instance()
		boss_1.position.y = -100
		boss_2.position.y = -100
		boss_1.position.x = rand_range(100, screen.x / 2 - 100)
		boss_2.position.x = rand_range(screen.x / 2, screen.x - 100)
		boss_1.connect("dying", self, "__reset_boss_timer")
		add_child(boss_1)
		add_child(boss_2)
	elif stage < 9:
		var boss : CreatureClass 
		boss = preload("res://scenes/game/boss2.tscn").instance()
		boss.position.y = -100
		boss.position.x = rand_range(100, screen.x - 100)
		boss.connect("dying", self, "__reset_boss_timer")
		add_child(boss)
	elif stage < 10:
		var boss_1 : CreatureClass 
		var boss_2 : CreatureClass 
		var boss_3 : CreatureClass 
		boss_1 = preload("res://scenes/game/boss1.tscn").instance()
		boss_2 = preload("res://scenes/game/boss1.tscn").instance()
		boss_3 = preload("res://scenes/game/boss1.tscn").instance()
		boss_1.position.y = -150
		boss_2.position.y = -100
		boss_3.position.y = -150
		boss_1.position.x = rand_range(100, screen.x / 3 - 100)
		boss_2.position.x = rand_range(screen.x / 3, screen.x / 3 * 2 - 100)
		boss_3.position.x = rand_range(screen.x / 3 * 2, screen.x - 100)
		boss_1.connect("dying", self, "__reset_boss_timer")
		add_child(boss_1)
		add_child(boss_2)
		add_child(boss_3)
	elif stage < 12:
		var boss_1 : CreatureClass 
		var boss_2 : CreatureClass 
		var boss_3 : CreatureClass 
		boss_1 = preload("res://scenes/game/boss1.tscn").instance()
		boss_2 = preload("res://scenes/game/boss2.tscn").instance()
		boss_3 = preload("res://scenes/game/boss1.tscn").instance()
		boss_1.position.y = -150
		boss_2.position.y = -100
		boss_3.position.y = -150
		boss_1.position.x = rand_range(100, screen.x / 3 - 100)
		boss_2.position.x = rand_range(screen.x / 3, screen.x / 3 * 2 - 100)
		boss_3.position.x = rand_range(screen.x / 3 * 2, screen.x - 100)
		boss_2.connect("dying", self, "__reset_boss_timer")
		add_child(boss_1)
		add_child(boss_2)
		add_child(boss_3)
		if stage == 11:
			var pickup = preload("res://scenes/game/pickup.tscn").instance()
			pickup.position.y = - 1000
			pickup.position.x = screen.x / 2
			add_child(pickup)
	elif stage < 13:
		var boss : CreatureClass 
		boss = preload("res://scenes/game/boss3.tscn").instance()
		boss.position.y = -200
		boss.position.x = screen.x - 40
		var boss2 : CreatureClass 
		boss2 = preload("res://scenes/game/boss1.tscn").instance()
		boss2.position.y = -700
		boss2.position.x = screen.x - 100
		boss2.connect("dying", self, "__reset_boss_timer")
		add_child(boss)	
		add_child(boss2)
	elif stage < 14:
		var boss : CreatureClass 
		boss = preload("res://scenes/game/boss3.tscn").instance()
		boss.position.y = -200
		boss.position.x = screen.x - 40
		var boss2 : CreatureClass 
		boss2 = preload("res://scenes/game/boss2.tscn").instance()
		boss2.position.y = -1100
		boss2.position.x = screen.x - 100
		boss2.connect("dying", self, "__reset_boss_timer")
		add_child(boss)	
		add_child(boss2)
	else:
		var boss : CreatureClass 
		boss = preload("res://scenes/game/boss3.tscn").instance()
		boss.position.y = -200
		boss.position.x = screen.x - 40
		add_child(boss)	
		var boss2 : CreatureClass 
		boss2 = preload("res://scenes/game/boss1.tscn").instance()
		boss2.position.y = -700
		boss2.position.x = screen.x - 100
		boss2.connect("dying", self, "__reset_boss_timer")
		add_child(boss2)
		var boss3 : CreatureClass 
		boss3 = preload("res://scenes/game/boss2.tscn").instance()
		boss3.position.y = -1300
		boss3.position.x = screen.x - 200
		boss3.connect("dying", self, "__reset_boss_timer")
		add_child(boss3)
	
func __reset_boss_timer():
	$BossTimer.start()
	stage += 1
	$UI/LabelStage.set_stage(stage)
	
func game_over():
	$AnimationPlayer.play("game_over")
	
func get_stage():
	return stage