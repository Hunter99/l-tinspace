extends Position2D

export var speed = 100
export var live = 100

var explosion

func _ready():
	explosion = preload("res://scenes/game/explosion.tscn")
	$Area2D/AnimationPlayerImage.play("idle")
	$Area2D/AnimationPlayerMovement.play("movement_1")
	$Area2D.connect("area_entered", self, "__collision")

func _process(delta):
	translate(Vector2.DOWN * speed * delta)
	
func damage(value):
	live -= value
	if live <= 0:
		get_node("/root/game/UI/LabelPoints").add_points(100)
		queue_free()
		
func __collision(area):
	if area.has_method("damage"):
		area.damage(20)
		
func get_live():
	return live