extends "res://scenes/game/CreatureClass.gd"

export(float) var live = 100
export var speed = 75

onready var laser = preload("res://scenes/game/enemy_shot_laser.tscn")

var screen = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))
var shoot_timer = Timer.new()

func _ready():
	$Area2D.connect("area_entered", self, "__collision")
	shoot_timer.connect("timeout", self, "shoot")
	shoot_timer.wait_time = 0.03
	add_child(shoot_timer)
	
func _process(delta):
	if live <= 0:
		die()
	if position.y < screen.y / 32:
		translate(Vector2.DOWN * speed * delta)
	else:
		if shoot_timer.is_stopped():
			shoot_timer.start()
		translate(Vector2.LEFT * speed / 6 * delta)
		if position.x <  -$Area2D/Sprite.get_rect().size.x / 2:
			queue_free()

func damage(value):
	live -= value / 1000.0
	
func get_live():
	return live
	
func shoot():
	var laser_instance = laser.instance()
	laser_instance.position = position + Vector2.DOWN * ($Area2D/Sprite.get_rect().size.y / 2 - 20)
	laser_instance.set_direction(Vector2.DOWN)
	get_parent().add_child(laser_instance)
	
func die():
	.die()
	get_node("/root/game/UI/LabelPoints").add_points(1000)
	queue_free()
	
func __collision(area):
	if area.has_method("get_type"):
		if area.get_type() == "Player":
			area.damage(200)