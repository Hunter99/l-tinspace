extends Area2D

export var speed = 200
export var shoot_wait_time = 0.1
export var damageable : bool = true
export var live = 100
export var screen_border = 30
export var shoot_level = 0

var laser
var shoot_timer : Timer
var shootable : bool = true
var screen = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))
var explosion
var dead = false

# Called when the node enters the scene tree for the first time.
func _ready():
	laser = preload("res://scenes/game/shot.tscn")
	shoot_timer = Timer.new()
	shoot_timer.connect("timeout", self, "__reset_shot")
	shoot_timer.wait_time = shoot_wait_time
	add_child(shoot_timer)
	damageable = true
	explosion = preload("res://scenes/game/explosion.tscn")

func _process(delta):
	
	var speed_frame_shift = 0
	if not dead:
		if Input.is_action_pressed("up") and position.y > screen_border:
			translate(Vector2.UP * speed * delta)
			speed_frame_shift = 1
		elif Input.is_action_pressed("down") and position.y < screen.y - screen_border:
			translate(Vector2.DOWN * speed * delta)
		
		if Input.is_action_pressed("left") and position.x > screen_border:
			$Sprite.flip_h = false
			$Sprite.frame = 2 + speed_frame_shift
			translate(Vector2.LEFT * speed * delta)
		elif Input.is_action_pressed("right") and position.x < screen.x - screen_border:
			$Sprite.flip_h = true
			$Sprite.frame = 2 + speed_frame_shift
			translate(Vector2.RIGHT * speed * delta)
		else:
			$Sprite.flip_h = false
			$Sprite.frame = 0 + speed_frame_shift
			
		if Input.is_action_pressed("action"):
			if shootable and damageable:
				shoot()
			
	if live <= 0 and not dead:
		get_parent().game_over()
		dead = true
		damageable = false
		$DamageAnimation.stop()
		$Sprite.visible = false
		$Livebar.visible = false
		
func __reset_shot():
	shootable = true
	shoot_timer.stop()
	
func damage(value):
	if damageable:
		add_child(explosion.instance())
		$DamageAnimation.play("damage")
		live -= value
		
func get_live():
	return live
	
func get_type():
	return "Player"
	
func shoot():
	if shoot_level == 0:
		var laser_instance = laser.instance()
		laser_instance.set_position(position + Vector2.UP * $Sprite.texture.get_size().y)
		laser_instance.set_direction(Vector2.UP)
		get_parent().add_child(laser_instance)
		shootable = false
		shoot_timer.start()
	if shoot_level == 1:
		var laser_instance_1 = laser.instance()
		var laser_instance_2 = laser.instance()
		var laser_instance_3 = laser.instance()
		laser_instance_1.set_position(position + Vector2.UP * $Sprite.texture.get_size().y)
		laser_instance_2.set_position(position + Vector2.UP * $Sprite.texture.get_size().y)
		laser_instance_3.set_position(position + Vector2.UP * $Sprite.texture.get_size().y)
		laser_instance_1.set_direction(Vector2.UP.rotated(-0.2))
		laser_instance_2.set_direction(Vector2.UP)
		laser_instance_3.set_direction(Vector2.UP.rotated(+0.2))
		laser_instance_1.mute()
		laser_instance_3.mute()
		get_parent().add_child(laser_instance_1)
		get_parent().add_child(laser_instance_2)
		get_parent().add_child(laser_instance_3)
		shootable = false
		shoot_timer.start()

func shoot_level_up():
	$powerup_sound.play()
	shoot_level += 1
		