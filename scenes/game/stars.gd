extends Sprite

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var speed = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	translate(Vector2.DOWN * speed * delta)
	
	var height = ProjectSettings.get("display/window/size/height")
	
	if position.y > height:
		position.y = -height * 2
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
