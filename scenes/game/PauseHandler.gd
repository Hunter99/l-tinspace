extends Node

func _ready() -> void:
	$PauseLabel.visible = false
	pass # Replace with function body.

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("pause"):
		$PauseLabel.visible = !get_tree().paused
		get_tree().paused = !get_tree().paused
		