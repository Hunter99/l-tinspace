extends "res://scenes/game/CreatureClass.gd"

export var live = 100
export var speed = 50

var second_phase = false
var screen = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))

func _ready():
	$Area2D.connect("area_entered", self, "__collision")
	$Area2D/AnimationPlayerIdle.play("idle_1")
	$ShotTimer.connect("timeout", self, "__shoot")
	$ShotTimer.start()

func _process(delta):
	if live <= 0:
		die()
	if live <= 50 and not second_phase:
		second_phase = true
		$Area2D/AnimationPlayerIdle.play("idle_2")
	if position.y < screen.y / 3:
		translate(Vector2.DOWN * speed * delta)

func get_live():
	return live
	
func damage(value):
	live -= value / 20
	
func __collision(area):
	if area.has_method("get_type"):
		if area.get_type() == "Player":
			area.damage(20)
			
func __shoot():
	var shoot = preload("res://scenes/game/enemy_shot.tscn").instance()
	shoot.position = position
	get_parent().add_child(shoot)
	var player = get_tree().get_root().find_node("Player", true, false)
	shoot.set_target(player.position)
	
func die():
	.die()
	get_node("/root/game/UI/LabelPoints").add_points(500)
	queue_free()