extends Area2D

export var speed = 100

func _ready():
	connect("area_entered", self, "__collision")
	pass # Replace with function body.

func _process(delta):
	translate(Vector2.DOWN * speed * delta)
	
func __collision(area):
	if area.has_method("get_type"):
		if area.get_type() == "Player":
			area.shoot_level_up()
			queue_free()