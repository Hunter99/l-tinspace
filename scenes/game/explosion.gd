extends Position2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("idle")
	$AudioStreamPlayer2D.play()
	get_node("/root/game/AnimationCamera").play("shaking")
	pass # Replace with function body.

func _process(delta):
	if not $AnimationPlayer.is_playing():
		queue_free()
