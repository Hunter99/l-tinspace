extends Area2D

export var speed = 800

var explosion
var direction : Vector2
var play_no_sound = false

# Called when the node enters the scene tree for the first time.
func _ready():
	explosion = preload("res://scenes/game/explosion.tscn")
	if !play_no_sound:
		$AudioStreamPlayer.play()
	connect("area_entered", self, "collision") 

func _process(delta):
	translate(direction * speed * delta)
	var dimension : Vector2 = Vector2(ProjectSettings.get("display/window/size/width"), ProjectSettings.get("display/window/size/height"))
	if position.x > dimension.x or position.x < 0 or position.y > dimension.y or position.y < 0:
		queue_free()
		
func collision(area : Area2D):
	if area.get_parent().has_method("damage"):
		var explosion_instance = explosion.instance()
		explosion_instance.position = position
		get_parent().add_child(explosion_instance)
		area.get_parent().damage(100)
		queue_free()
		
func set_position(start_pos):
	position = start_pos
	
func set_direction(direction : Vector2):
	self.direction = direction
	
func mute():
	play_no_sound = true