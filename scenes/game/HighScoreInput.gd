extends AcceptDialog

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var cancel_button : Button = add_cancel("Cancel")
	connect("confirmed", self, "ok")
	cancel_button.connect("button_down", self, "cancel")
	popup_exclusive = true
	register_text_enter($Container/LineEdit)
	$HTTPRequest.connect("request_completed", self, "__on_request_ready")
	pass # Replace with function body.

func cancel():
	get_tree().change_scene("res://menu.tscn")

func ok():
	var stage = get_parent().get_parent().get_stage()
	var score = get_parent().get_node("LabelPoints").get_score()
	var name = $Container/LineEdit.text
	name = name.replace(" ", "")
	name = name.replace("'", "")
	name = name.replace("/", "")
	name = name.replace(".", "")
	name = name.replace("&", "")
	name = name.replace("=", "")
	$HTTPRequest.request("https://line.hunter99.ch/game/score.php?action=insert&name=" + str(name) + "&score=" + str(score) + "&stage=" + str(stage))
	

func __on_request_ready(result, response_code, headers, body):
	print(body.get_string_from_utf8())
	get_tree().change_scene("res://menu.tscn")
	pass