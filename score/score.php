<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    include 'dbhandler.php';
    
    $db_handler = new DBHandler();
    $db_handler->setDatabase('score');
    if(isset($_GET['action'])){
        if($_GET['action'] == "insert"){
            if(isset($_GET['name']) && isset($_GET['score']) && isset($_GET['stage'])){
                $name = $_GET['name'];
                $score = $_GET['score'];
                $stage = $_GET['stage'];
                checkAndChangeForCorrectInput($name);
                checkAndChangeForCorrectInput($score, "integer");
                checkAndChangeForCorrectInput($stage, "integer");
                
                $parameters = array(
                    "name" => $name,
                    "score" => $score,
                    "stage" => $stage,
                );

                $db_handler->insertData("score", $parameters);
        
                print("OK");
            }
        }

        if($_GET['action'] == "get"){
            $rows = $db_handler->getRows("score", "score");
            $i = 0;
            foreach(array_reverse($rows) as $row){
                print($row['name']."\n");
                print($row['score']."\n");
                print($row['stage']."\n");
                $i++;
                if($i > 10){
                    break;
                }
            }
        }
    }

    function checkAndChangeForCorrectInput(&$value, $type = "string"){
        if(preg_match('/"/', $value)){
            return false;
        }
        if(preg_match("/'/", $value)){
            return false;
        }
        
        if($type === "string"){
            if(!ctype_print($value)){
                return false;
            }
        }
        if($type === "integer"){
            if(!is_numeric($value)){
                return false;
            }
            settype($value, "integer");
        }
        if($type === "float"){
            if(!is_numeric($value)){
                return false;
            }
            settype($value, "float");
        }

        return true;
    }
?>