<?php 
    class DBHandler{
        private $db;

        /**
         * Sets the SQLite databes name
         * @param object @db_file_name The name of the SQLite file.
         */
        public function setDatabase($db_file_name){
            $this->db = new SQLite3($db_file_name);
        }

        /**
         * Inserts data in a specific table
         * 
         * @param object $table The table name
         * @param array $parameters Array with parameter names and values -> "parameter_name" => value
         */
        public function insertData($table_name, $parameters){
            list($parameter_names, $values) = $this->seperateParameters($parameters);
            $this->db->exec('BEGIN');
            $this->db->exec('INSERT INTO '.$table_name.' ('.$parameter_names.') VALUES ('.$values.')');
            $this->db->exec('COMMIT');
        }

        /**
         * Updates all or a specifig value in a table
         * @param object $table The table name.
         * @param object $parameter The parameter name.
         * @param object $value The value.
         * @param array $where_parameters (optional) Array of all WHERE parameters eg. "id" => 10.
         */
        public function updateValue($table_name, $parameter, $value, $where_parameters = null){
            $this->db->exec('BEGIN');
            if(is_null($where_parameters)){
                $this->db->exec('UPDATE '.$table_name.' SET '.$parameter.' = '.$value);
            } else {
                $where_string = $this->seperateWhere($where_parameters);
                $this->db->exec('UPDATE '.$table_name.' SET '.$parameter.' = '.$value.' WHERE '.$where_string);
            }
            $this->db->exec('COMMIT');
        }

        /**
         * Removes all values or a specifig row in a table
         * @param object $table The table name.
         * @param array $where_parameters (optional) Array of all WHERE parameters eg. "id" => 10.
         */
        public function remove($table_name, $where_parameters = null){
            $this->db->exec('BEGIN');
            if(is_null($where_parameters)){
                $this->db->exec('DELETE FROM '.$table_name);
            } else {
                $where_string = $this->seperateWhere($where_parameters);
                $this->db->exec('DELETE FROM '.$table_name.' WHERE '.$where_string);
            }
            $this->db->exec('COMMIT');
        }

        /**
         * Loads values of a specific row
         * @param object $table The table name.
         * @param array $where_parameters (optional) Array of all WHERE parameters eg. "id" => 10.
         * @return array array of columns
         */
        public function getRow($table_name, $where_parameters = null){
            if(is_null($where_parameters)){
                $result = $this->db->query('SELECT * FROM '.$table_name);
            } else {
                $where_string = $this->seperateWhere($where_parameters);
                $result = $this->db->query('SELECT * FROM '.$table_name.' WHERE '.$where_string);
            }
            return $result->fetchArray();
        }

        /**
         * Loads all rows
         * @param object $table The table name.
         * @return array array of columns
         */
        public function getRows($table_name, $order_row = null){
            if(is_null($order_row)){
                $result = $this->db->query('SELECT * FROM '.$table_name);
            } else {
                $result = $this->db->query('SELECT * FROM '.$table_name.' ORDER BY '.$order_row);
            }
            $rows = array();
            while($row = $result->fetchArray()){
                array_push($rows, $row);
            }
            return $rows;
        }

        /**
         * Loads last N rows
         * @param object $table The table name.
         * @param object $n Number of the last rows.
         * @return array array of columns
         */
        public function getLastNRows($table_name, $n){
            $result = $this->db->query('SELECT * FROM '.$table_name.' ORDER BY id DESC LIMIT '.$n);
            $rows = array();
            while($row = $result->fetchArray()){
                array_push($rows, $row);
            }
            return $rows;
        }

        /**
         * Returns the number of rows in a table
         * @param object $table The table name.
         * @return int Number of Rows
         */
        public function getNumberOfRows($table_name){
            $result = $this->db->query('SELECT * FROM '.$table_name);
            $number_rows = 0;
            while ($result->fetchArray()){
                $number_rows++;
            }
            $result->reset();
            return $number_rows;
        }

        /**
         * Seperates parameter array to parameter and value string
         * @param array $parameters Array with parameter names and values -> "parameter_name" => value
         * @return array The seperated parameters and values as string in a array -> $parameter_string, $value_string
         */
        private function seperateParameters($parameters){
            $parameter_string = '"';
            $value_string = '"';
            $index = 0;
            foreach ($parameters as $key => $value){
                if($index != 0){
                    $parameter_string = $parameter_string . '", "';
                    $value_string = $value_string . '", "';
                }
                $parameter_string = $parameter_string . $key;
                $value_string = $value_string . $value;
                $index = $index + 1;
            }

            $parameter_string = $parameter_string . '"';
            $value_string = $value_string . '"';

            return array($parameter_string, $value_string);
        }

        /**
         * Converts a WHERE parameter array to a string with AND between 
         * @param object $where_parameters WHERE array with the parameters
         * @return string Converted string of the WHERE parameter
         */
        private function seperateWhere($where_parameters){
            $where_string = '';
            $index = 0;
            foreach ($where_parameters as $key => $value){
                if($index != 0){
                    $where_string = $where_string . 'AND ';
                }
                $where_string = $where_string . $key . ' = ' . '"'.$value.'"';
                $index = $index + 1;
            }

            return $where_string;
        }
    }
?>